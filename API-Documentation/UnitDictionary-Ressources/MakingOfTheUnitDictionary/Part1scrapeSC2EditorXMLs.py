
# coding: utf-8

# Man kann ausführliche Spielwerte durch den Starcraft 2 - Editor gewinnen. Dieser stellt im Data-Fenster die Eigenschaften der Units für jedes Update etc als XML bereit.
# Die Kunst ist diese Sinnvoll in Python zu verwenden.
# 
# Hier wird die Dokumentation einer Klasse, welche die relevanten Daten für SC2 bereitstellen kann, beschrieben.
# Als erstes das parsen der XML-Texte aus dem Editor (welche manuell kopiert wurden):
# 

# In[2]:


from bs4 import BeautifulSoup
import os
import sys
import re
import pickle


# # Unit Scraping entwickeln

# In[3]:


with open(r"unitsliberty.txt","r") as f:
    text = f.read()


# In[3]:


soup = BeautifulSoup(text)


# In[4]:


units = soup.findAll('cunit')


# In[5]:


print(len(units))


# In[6]:


print(units[2])


# In[7]:


unit2 = units[2]


# In[8]:


print(unit2.attrs)


# In[9]:


for i in unit2.children:
    print(i)


# In[40]:


child = unit2.contents[5]


# In[41]:


print(child)


# In[66]:


child.attrs


# In[87]:


def parsecontentlines(contentlines):
    contentdic = {}
    for contentline in contentlines:
        newdic = {}
        try:
            #will fail if no attribute "index"
            if contentline.attrs['index']:
                # we have a dictionary (index,value)

                newdic[contentline.attrs['index']] = contentline.attrs['value']
                try:
                    # do we already have a dic?
                    if isinstance(contentdic[contentline.name],(dict)):
                        contentdic[contentline.name][contentline.attrs['index']] = contentline.attrs['value']
                    else:
                        # do we have a single value?
                        newdic[contentdic[contentline.name]] = contentdic[contentline.name]
                        contentdic[contentline.name] = newdic
                except:
                    # we create a new keyentry
                    contentdic[contentline.name] = newdic
        except:
            try:
                
                #we have no index attribute
                if str(contentline.name).find('array')+1:
                    if len(contentline.attrs.values()) > 1:
                        newdic = contentline.attrs
                        try:
                            # do we already have a dic?
                            if isinstance(contentdic[contentline.name],(dict)):
                                # does the dic already have our keys?
                                collisionnumber = 1
                                for  key in contentline.attrs.keys():
                                    try:
                                        contentdic[contentline.name][key]
                                        hasthekey = True
                                        testcollision = True
                                        while (testcollision):
                                            collisionnumber += 1
                                            try:
                                                contentdic[contentline.name][str(key)+str(collisionnumber)]
                                            except:
                                                testcollision = False
                                    except:
                                        pass
                                if hasthekey:
                                    for key in contentline.attrs.keys():
                                        contentdic[contentline.name][str(key)+str(collisionnumber)] = contentline.attrs[key]
                            else:
                                # do we have a single value?
                                newdic[contentdic[contentline.name]] = contentdic[contentline.name]
                                contentdic[contentline.name] = newdic
                        except:
                            # we create a new keyentry
                            contentdic[contentline.name] = newdic
                    newdic = {}
                    for  value in contentline.attrs.values():
                        newdic[value] = 1
                    try:
                        # do we already have a dic?
                        if isinstance(contentdic[contentline.name],(dict)):
                            for  value in contentline.attrs.values():
                                contentdic[contentline.name][value] = 1
                        else:
                            # do we have a single value?
                            newdic[contentdic[contentline.name]] = contentdic[contentline.name]
                            contentdic[contentline.name] = newdic
                    except:
                        # we create a new keyentry
                        contentdic[contentline.name] = newdic
                else:
                    #only one value
                    for i in contentline.attrs.values():
                        contentdic[contentline.name] = i
            except:
                if len(contentline) > 2:
                    print()
                    print(sys.exc_info()[0:2])
                    print(contentline)
    return contentdic
            


# In[1]:


def insertOrModify(finaldic, insert):
        inlist = False
        for finkey in finaldic.keys():
            if finkey == insert['id']:
                inlist = True
                
        if inlist:
            for key in insert.keys():
                try:
                    oldval = finaldic[insert['id']][key]
                    try:
                        for i in insert[key].keys():
                            oldval[i] = insert[key][i]
                    except:
                        finaldic[insert['id']][key] = insert[key]
                except:
                    finaldic[insert['id']][key] = insert[key]
        else:
            name = insert['id']
            finaldic[name] = insert


# # scraping UNits

# In[89]:


suffixlist = ["liberty", "swarm","void"]
finalunits = {}
for suffix in suffixlist:
    filename = "units"+suffix+".txt"
    with open(filename,"r") as f:
        text = f.read()
    soup = BeautifulSoup(text)
    units = soup.findAll('cunit')
    scrapedunits = []
    for unit in units:
        rows = parsecontentlines(unit.contents)
        try:
            rows['id'] = unit.attrs['id']
        except:
            continue
        scrapedunits.append(rows)
    print('### INITIAL SCRAPING DONE ###')
   
    for unit in scrapedunits:
        try:
            cat = unit['editorcategories']
            if not cat.find("Meele"):
                continue
        except:
            pass
        
        ## test if previous update has already created this unit. just do modifications
        insertOrModify(finalunits,unit)


# In[90]:


len(finalunits)


# In[91]:


finalunits['StarportReactor']


# # Entwickeln des Abilityscapings

# In[228]:


with open(r"abilityliberty.txt","r") as f:
    text = f.read()


# In[229]:


soup = BeautifulSoup(text)


# In[270]:


abilities = soup.findAll(re.compile('^cabil'))


# In[271]:


len(abilities)


# In[277]:


ab1 = abilities[1]


# In[280]:


ab1.contents


# In[296]:


scrapedabils = []
for abil in abilities:
    row = {}
    try:
        row['id'] = [abil.attrs['id']]
    except:
        print(abil)
        continue
    for attr in abil.contents:
        if attr.name in row.keys():
            try:
                a = []
                for i in attr.attrs.items():
                    a.append(i)
                row[attr.name].append(a)
            except:
                if len(attr) > 2:
                    print(attr)
        else:
            try:
                a = []
                for i in attr.attrs.items():
                    a.append(i)
                row[attr.name] = [a]
            except:
                if len(attr) > 2:
                    print(attr)
    scrapedabils.append(row)


# In[297]:


scrapedabils[18]


# # scraping Abilities

# In[98]:


finabils = {}
for suffix in suffixlist:
    filename = "ability"+suffix+".txt"
    with open(filename,"r") as f:
        text = f.read()
    soup = BeautifulSoup(text)
    abilities = soup.findAll(re.compile('^cabil'))
    scrapedabils = []
    for abil in abilities:
        row = parsecontentlines(abil.contents)
        try:
            row['id'] = abil.attrs['id']
        except:
            print(abil)
            continue
        scrapedabils.append(row)
    print('### INITIAL SCRAPING DONE ###')
   
    for abil in scrapedabils:
        try:
            insertOrModify(finabils,abil)
        except:
            print()
            print(abil)


# In[99]:


len(finabils)


# In[100]:


print(finabils.keys())


# In[101]:


print(finabils['Repair'])


# # Entwickeln des Weaponscrapings

# In[311]:


with open(r"weaponsliberty.txt","r") as f:
    text = f.read()


# In[312]:


soup = BeautifulSoup(text)


# In[313]:


text


# In[314]:


weapons = soup.findAll(re.compile('^cweapon'))


# In[315]:


len(weapons)


# In[318]:


we1 = weapons[50]


# In[319]:


print(we1)


# # scraping Weapons

# In[104]:


finweapons = {}
for suffix in suffixlist:
    filename = "weapons"+suffix+".txt"
    with open(filename,"r") as f:
        text = f.read()
    soup = BeautifulSoup(text)
    scrapedweapons = []
    weapons = soup.findAll(re.compile('^cweapon'))
    for wpn in weapons:
        row = parsecontentlines(wpn.contents)
        try:
            row['id'] = wpn.attrs['id']
        except:
            continue
        scrapedweapons.append(row)
    print('### INITIAL SCRAPING DONE ###')
    
    for wpn in scrapedweapons:
        try:
            insertOrModify(finweapons,wpn)
        except:
            print(wpn)


# In[105]:


finweapons.keys()


# In[106]:


finweapons['PrismaticBeam']


# # Entwickeln des Effects-scraping

# In[332]:


with open(r"effectsliberty.txt","r") as f:
    text = f.read()
soup = BeautifulSoup(text)
text


# In[333]:


effects = soup.findAll(re.compile('^ceffect'))
len(effects)


# In[336]:


ef1 = effects[39]
print(ef1)


# # Scraping Effects

# In[108]:


fineffects = {}
for suffix in suffixlist:
    filename = "effects"+suffix+".txt"
    with open(filename,"r") as f:
        text = f.read()
    soup = BeautifulSoup(text)
    scraped = []
    effects = soup.findAll(re.compile('^ceffect'))
    for scr in effects:
        row = parsecontentlines(scr.contents)
        try:
            row['id'] = scr.attrs['id']
        except:
            print(scr)
            continue
        scraped.append(row)
    print('### INITIAL SCRAPING DONE ###')

    for scr in scraped:
        try:
            insertOrModify(fineffects,scr)
        except:
            print()
            print(scr)


# In[109]:


len(fineffects)


# In[110]:


print(fineffects.keys())


# In[111]:


print(fineffects['SalvageZergling'])


# # Entwickeln scraping Upgrade

# In[350]:


with open(r"upgradesliberty.txt","r") as f:
    text = f.read()
soup = BeautifulSoup(text)
text


# In[351]:


upgrades = soup.findAll(re.compile('^cupgrade'))
len(upgrades)


# In[352]:


up1 = upgrades[39]
print(up1)


# # Scraping Upgrades

# In[112]:


finupgrades = {}
for suffix in suffixlist:
    filename = "upgrades"+suffix+".txt"
    with open(filename,"r") as f:
        text = f.read()
    soup = BeautifulSoup(text)
    scraped = []
    upgrades = soup.findAll(re.compile('^cupgrade'))
    for scr in upgrades:
        row = parsecontentlines(scr.contents)
        try:
            row['id'] = scr.attrs['id']
        except:
            print(scr)
            continue
        scraped.append(row)
    print('### INITIAL SCRAPING DONE ###')
   
    for scr in scraped:
        try:
            insertOrModify(finupgrades,scr)
        except:
            continue


# In[113]:


len(finupgrades)


# In[114]:


print(finupgrades.keys())


# In[115]:


print(finupgrades['TerranInfantryWeapons'])


# # Save Files

# In[116]:


import pickle
with open("pickledUnitsEffectsAbilitiesWeaponsUpgrades","wb") as f:
    pickle.dump([finalunits,fineffects,finabils,finweapons,finupgrades],f)


## API-Dokumentation
Dieser Ordner enthält alle wichtigen Informationen die wir über die API
zusammentragen.

## Links
Basic Guide about Starcraft:
http://learningsc2.com/tag/control-groups/

Tutorial Basic Agent:
https://chatbotslife.com/building-a-smart-pysc2-agent-cdc269cb095d

Tutorial Smart Agent:
https://chatbotslife.com/building-a-smart-pysc2-agent-cdc269cb095d

Learned Build Order Recommender System:
http://cole-maclean.github.io/blog/Evolving%20the%20StarCraftII%20Build%20Order%20Meta/

## Installation

### Windows:
1. Python installieren, am besten via Anaconda.
2. Anaconda Prompt öffnen
3. pip3 install pysc2

### Linux & macOS:
1. Sicherstellen das Python3 installiert ist.
2. pip3 install pysc2

### Agent starten:
Um den Beispielagenten ausführen zu können muss in Starcraft ein Ordner 'Maps' 
angelegt werden. In diesen müssen die 'Meele' Karten von Blizzard kopiert werden.

python3 -m pysc2.bin.agent --map Simple64


Installation via Git:
$ git clone https://github.com/deepmind/pysc2.git
$ pip install pysc2/


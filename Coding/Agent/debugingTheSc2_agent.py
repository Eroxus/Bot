#python -m pysc2.bin.agent --map Simple64 --agent pysc2.agents.Agent.sc2_agent_debug.Test --agent_race T
"""
Created on Fri Dec 15 22:41:40 2017

@author: JHMLap
"""
import socket
import pickle
from pysc2.agents.Agent.sc2_agent import SC2Agent
import sys

# Class for separating the Agent from the SC2 instance - useful for Debugging with an IDE while communicating with SC2
# THE SC2_AGENT_DEBUG HAS TO BE RUNNING WITH SC2 !!! (Like the usual Agent)
class StartDebug:
    # Functions for Debugging an SC2-external instance of the Agent - it has to be startet AFTER the SC2 Instance is up and running.
    #region Debugging Functions
    # gets a connection set up to the DebugAgent that is startet along with Starcraft2, also starts our SC2Agent
    def __init__(self):
        print("StartDebug: The sc2_agent_debug and an SC2 Instance have to be running! make sure you have startet it with the Console command 'python -m pysc2.bin.agent --map Simple64 --agent pysc2.agents.Agent.sc2_agent_debug.Test --agent_race T'")
        print()
        host = socket.gethostname()
        port = 999
        self.connection = socket.socket()
        self.connection.connect(('localhost',port))
        self.agent = SC2Agent()
        self.PreviousObs = []
        self.PreviousActions = []
        self.connection.send(" ".encode())
        f = self.connection.makefile('rb', 32768 )
        try:
          self.specs = pickle.load(f)
        except EOFError:
          self.specs = None
        f.close()
        try:
           self.agent.setup(self.specs,None)
        except:
           self.agent = None
           print("StartDebug: Setup of the SC2Agent failed. Fix that and issue my restartAgent() command")
        print("StartDebug: I may have established a Connection")
    #Debugging Function for reading external Observations
    def _readExternalObs(self):
        self.connection.send(" ".encode())
        f = self.connection.makefile('rb', 32768 )
        try:
          data = pickle.load(f)
        except EOFError:
          return None
        f.close()
        self.PreviousObs.append(data)
        return data
    
    #Debugging Function for issuring Commands to an external SC2 instance
    def _writeExternalCommand(self, data):
        self.PreviousActions.append(data)
        f = self.connection.makefile('wb', 32768 )
        pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)
        f.close()
    #If our Agent is not doing well or crashes it may be restartet without having to restart the whole SC2 Session
    def restartAgent(self):
        failedRestore = False
        try:
           self.agent.setup(self.specs, None)
        except:
           self.agent = None
           print("StartDebug: Setup of the SC2Agent failed. Fix that and issue my restartAgent() command")
           print(sys.exc_info()[0])
        for step in range(len(self.PreviousObs)):
           try:
              action = self.agent.step(self.PreviousObs[step])
              if not action.equals(self.PreviousActions[step]):
                 print("StartDebug: an Action has changed from its Previous Agents Orders - you may want to restart the SC2-session and me")
           except:
              print("StartDebug: Something went wrong at Step: "+str(step)+" - you should fix that and issue my restartAgent() command")
              print(sys.exc_info()[0])
              self.agent = None
              failedrestore = True
              break
           if not failedRestore:
             print("StartDebug: Agent restored to the last remembered Step")
    
    #This Function triggers one new Step for the Agent
    def Step(self):
        obs =  self._readExternalObs()
        try:
           action = self.agent.step(obs)
           self._writeExternalCommand(action)
        except:
           print("StartDebug: Agent Crashed - you should fix that and issue my restartAgent() command")
           print(sys.exc_info()[0])
           self.agent = None
           self._writeExternalCommand([0,[]])

    #endregion

if __name__ == "__main__":
    debug = StartDebug()
    while True:
      debug.Step()
import csv
import os
import pysc2.agents.UnitDictionary.unitDictionary as dictionary
#from pysc2.agents.Agent.sc2_agent import OBJECT_TYPE

# Filename & Path
PATH = os.path.abspath(os.path.dirname(__file__))
FILENAME = {'PREGAME' : 'BuildOrder_PREGAME.csv',
			'ATTACK' : 'BuildOrder_ATTACK.csv',
			'DEFEND' : 'BuildOrder_DEFEND.csv',
			'REBUILD' : 'BuildOrder_REBUILD.csv',
			'EXPAND' : 'BuildOrder_EXPAND.csv'}
# Headers in CSV
HEADER_NAME = 'Name'
HEADER_SUPPLY = 'Supply'
HEADER_UPGRADE = 'Upgrade'
HEADER_BASE = ''

# Supply Constants
SUPPLY_DIFFERENCE_PREGAME = 4
SUPPLY_DIFFERENCE_ATTACK = 5
SUPPLYDEPOT_NAME = 'SupplyDepot'

# Vars
currentList = 0
waitForSupplyDepot = False
lastSupply = 0
lists = []
debugOutputEnabled = False

class BuildorderLists:
	PREGAME = 0
	ATTACK = 1
	#DEFEND = 2
	#REBUILD = 3
	#EXPAND = 4

class bo(object):
	def __init__(self, name, buildorderId, buildorder):
		self.name = name
		self.id = buildorderId
		self.buildorder = buildorder
		self.currentLine = 0

# Reads the Buildorder from the given Path
def readBuildorder(path, filename):

	with open(os.path.join(path , filename), "r") as file:
	    reader = csv.DictReader(file, delimiter= ';')
	    return( list(reader) )

def getNextUnitId(supplyUsed, supplyMax):
	global currentList, lists

	finished = False

	current = list(filter(lambda l : l.id == currentList, lists))[0]

	while not finished:
		if currentList == BuildorderLists.PREGAME:
			(finished, unit) = _statePregame(current, supplyUsed, supplyMax)

		elif currentList == BuildorderLists.ATTACK:
			(finished, unit) = _stateAttack(current, supplyUsed, supplyMax)

		if debugOutputEnabled:
			print(finished)
			print(unit)

	return unit


def _statePregame(current, supplyUsed, supplyMax):
	global currentList, lastSupply, waitForSupplyDepot
	unitId = [ None, None, 1]

	if current.currentLine < len(current.buildorder):

		tempLine = current.currentLine
		tempId = dictionary.getUnitIdByUnitName( current.buildorder[current.currentLine][HEADER_NAME] )

		if(dictionary.getObjectTypeById(tempId) == 'Unit'):

			if(lastSupply < supplyMax):
				waitForSupplyDepot = False

			tempFood = dictionary.getFoodByName( current.buildorder[current.currentLine][HEADER_NAME] )
			tempSupply = supplyUsed

			# While: we're not at the end of the list AND the next unit is the same as the current unit AND we dont need more supplyDepots
			while (tempLine < len(current.buildorder) and dictionary.getUnitIdByUnitName( current.buildorder[tempLine][HEADER_NAME]) == tempId and ((tempSupply < supplyMax - SUPPLY_DIFFERENCE_PREGAME and not waitForSupplyDepot) or (tempSupply < supplyMax and waitForSupplyDepot)) ):
				tempLine = tempLine + 1
				tempSupply = tempSupply + tempFood

			unitQuantity = tempLine - current.currentLine

			# set the currentLine to the templine
			current.currentLine = tempLine

			# if unitQuantity is 0, we dont have enough supply
			if(unitQuantity == 0 and not waitForSupplyDepot):
				unitId[0] = dictionary.getUnitIdByUnitName( SUPPLYDEPOT_NAME )
				waitForSupplyDepot = True
				lastSupply = supplyMax
			elif(unitQuantity == 0 and waitForSupplyDepot):
				return (True, None)
			else:
				if debugOutputEnabled:
					print('Build Units ' + str(tempId) + ' ' + str(unitQuantity))
				unitId[0] = tempId
				unitId[2] = unitQuantity


		else:
			unitId[0] = dictionary.getUnitIdByUnitName( current.buildorder[current.currentLine][HEADER_NAME] )

			if current.buildorder[current.currentLine][HEADER_UPGRADE]:
				unitId[1] = dictionary.getUnitIdByUnitName( current.buildorder[current.currentLine][HEADER_UPGRADE] )

			setCurrentIndex( current.currentLine + 1 )

		return(True, unitId)

	else:
		currentList = BuildorderLists.ATTACK
		if debugOutputEnabled:
			print('BUILDORDER: Change list to Attacklist. End of Pregame.')
		return (False, None)

def _stateAttack(current, supplyUsed, supplyMax):
	global currentList
	unitId = [ None, None, 1]

	if current.currentLine < len(current.buildorder):

		# Build SupplyDepot, if we need it
		if(supplyUsed >= supplyMax - SUPPLY_DIFFERENCE_ATTACK):
			unitId[0] = dictionary.getUnitIdByUnitName( SUPPLYDEPOT_NAME )

		else:
			unitId[0] = dictionary.getUnitIdByUnitName( current.buildorder[current.currentLine][HEADER_NAME] )

			if current.buildorder[current.currentLine][HEADER_UPGRADE]:
				unitId[1] = dictionary.getUnitIdByUnitName( current.buildorder[current.currentLine][HEADER_UPGRADE] )

			setCurrentIndex( current.currentLine + 1 )

		return(True, unitId)

	else:
		current.currentLine = 0;
		return (False, None)



# Returns the recommended used Supply for Building/Training the current Unit.
def getCurrentSupply():
	current = list(filter(lambda l : l.id == currentList, lists))[0]

	if current.currentLine < len(current.buildorder):
		supply = int(current.buildorder[current.currentLine][HEADER_SUPPLY])
	else:
		supply = 0
	return supply

# Returns the index of the Buildorder
def getCurrentIndex():
	return (list(filter(lambda l : l.id == currentList, lists))[0].currentLine)

# Sets the index of the Buildorder
def setCurrentIndex(lineIndex):
	global lists
	list(filter(lambda l : l.id == currentList, lists))[0].currentLine = lineIndex


# Set the List of the Buildorder
def setBuildorderList(listId):
	global currentList
	currentList = listId

# Initializes the Module
def initModule():
	global lists

	# Iterating through all given listnames
	for name in filter(lambda a: not a.startswith('__'), dir(BuildorderLists)):
		lists.append(bo(name, getattr(BuildorderLists, name), readBuildorder( PATH, FILENAME[name])))



# This Code is only executed once on the module import.
initModule()

# For Testing
#
# Zum Testen folgenden Befehl im Ordner Coding ausführen:
# python -m Buildorder.Buildorder.buildorder
#
if __name__ ==  '__main__':

	#Zugriffsbeispiel
	print(buildorder[5][HEADER_NAME])
	print(buildorder[5][HEADER_SUPPLY])
	print(getNextUnitId())
